FROM openjdk:oraclelinux8
LABEL authors="doducmanh"
ARG FILE_JAR=target/build-devops.jar
COPY ${FILE_JAR} application.jar
ENTRYPOINT ["java", "-jar", "/application.jar"]