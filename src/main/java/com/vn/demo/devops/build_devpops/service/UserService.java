package com.vn.demo.devops.build_devpops.service;

import com.vn.demo.devops.build_devpops.api.UserApi;
import com.vn.demo.devops.build_devpops.dto.UserRequest;
import com.vn.demo.devops.build_devpops.dto.UserResponse;
import com.vn.demo.devops.build_devpops.entity.UserEntity;
import com.vn.demo.devops.build_devpops.mapper.UserMapper;
import com.vn.demo.devops.build_devpops.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserApi {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    public static Integer valueA;

    @Override
    public void create(UserRequest request) {
        userRepository.save(userMapper.mapToUserEntity(request));
    }

    @Override
    public List<UserResponse> getUsers() {
        log.info("Id random is {}", valueA);
        return userRepository.findAll().stream().map(userMapper::mapToUserResponse).collect(Collectors.toList());
    }

    @Override
    public UserResponse getUser(Long userId) {

        Random random = new Random();

        valueA = random.nextInt(10000);

        return userRepository.findById(userId).map(userMapper::mapToUserResponse)
                .orElse(null);
    }

    @Override
    public void deleteUser(Long userId) {

    }

    @Override
    public void updateUser(UserEntity user, Long userId) {

    }
}
